package org.example;

import com.codeborne.selenide.Condition;
import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;

import java.time.Duration;
import java.util.List;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class SearchPage {

    public static By openMapButton = By.xpath("//div[@data-testid='map-trigger']");
    public static By firstHotel = By.xpath("//a[@data-testid='property-list-map-card' or contains(@class, 'map-card__container')]");
    public static By labelHotelName = By.xpath("//span[@data-testid='header-title' or contains(@class, 'map-card__title-link')]");
    public static By labelHotelStarsCount = By.xpath("//a[@data-testid='property-list-map-card' or contains(@class, 'map-card__container')][1]//span[@data-testid='rating-stars' or contains(@class, 'bui-rating bui-rating--smaller') or @data-testid='rating-circles']");
    public static By labelHotelAverageRating = By.xpath("//div[@data-testid='review-score-right-component']/div[1]|//div[@data-testid='header-container']/../div[2]/div/div[1]|//div[contains(@class, 'bui-review-score__badge')]");
    public static By labelHotelReviewsCount = By.xpath("//div[@data-testid='review-score-right-component']/div[2]/div[2]|//div[@data-testid='header-container']/../div[2]/div/div[2]/div[2]|//div[contains(@class, 'bui-review-score__text')]");
    public static By labelHotelCost = By.xpath("//div[@data-testid='price-and-discounted-price']|//div[contains(@class, 'bui-price-display__value.prco-text-nowrap-helper prco-inline-block-maker-helper')]|//span[contains(@class, 'prco-valign-middle-helper')]");
    public static By jumpingMarker = By.cssSelector(".svg-marker.svg-poi.atlas-marker.hotel.fading.bounce");
    public static By datePicker = By.cssSelector("div[data-testid='searchbox-datepicker-calendar']");
    public static By inDate = By.cssSelector("div[data-testid='searchbox-checkin-container']");
    public static By outDate = By.cssSelector("div[data-testid='searchbox-checkout-container']");
    public static By popularFilters = By.cssSelector("div[data-testid='filters-group']:nth-child(2) div[data-filters-item]");
    public static By popularFiltersBlock = By.cssSelector("div[data-filters-group='popular_sticky']");
    public static By hotelsCount = By.xpath("//div[@data-component='arp-header']//h1");
    public static By sortButton = By.cssSelector("button[data-testid='sorters-dropdown-trigger']");
    public static By descSortButton = By.xpath("//button[@data-id='class']");
    public static By firstCardLink = By.xpath("//div[@data-testid='property-card'][1]//a[@data-testid='title-link']");
    public String hotelName, hotelStarsCount, hotelAverageRating, hotelReviewsCount, hotelCost;

    public SearchPage openMap() {
        $(openMapButton).click();
        return this;
    }

    public SearchPage moveMouseToFirstHotel() {
        $(firstHotel).shouldBe(Condition.enabled, Duration.ofSeconds(20)).hover();
        return this;
    }

    public SearchPage initHotelDataFromMap() {
        hotelName = $(labelHotelName).text();
        try {
            hotelStarsCount = String.valueOf($(labelHotelStarsCount).$$("span").size());
        } catch (NoSuchElementException e) {
            hotelStarsCount = "0";
        }
        hotelAverageRating = $(labelHotelAverageRating).text();
        hotelReviewsCount = $(labelHotelReviewsCount).text();
        var element = $(labelHotelCost).$$("span");
        if (element.size() == 2) {
            hotelCost = element.get(1).text();
        } else {
            hotelCost = element.get(0).text();
        }
        return this;
    }

    public SearchPage clickJumpingMarker() {
        $(jumpingMarker).shouldBe(Condition.enabled).click();
        return this;
    }

    public SearchPage inputDateAfterVillasPage(String start, String end) {
        $(datePicker).find(By.cssSelector("span[data-date='%s']".formatted(start))).click();
        $(outDate).click();
        $(datePicker).find(By.cssSelector("span[data-date='%s']".formatted(end))).click();
        return this;
    }

    public SearchPage setFilters(List<Integer> indexes) {
        var filters = $$(popularFilters);
        $(popularFiltersBlock).scrollTo();
        for (int index : indexes) {
            filters.get(index).shouldBe(Condition.enabled).click();
        }
        return this;
    }

    public SearchPage checkVillasCounter(Integer firstFamousPageCounter, SoftAssertions assertions) {
        var beforeCounter = Integer.valueOf($(hotelsCount)
                .text().replaceAll("[^0-9]", ""));
        assertions.assertThat(beforeCounter).as("Неправильное количество отелей")
                .isEqualTo(firstFamousPageCounter);
        return this;
    }

    public SearchPage sortByGradeDescending() {
        $(sortButton).click();
        $(descSortButton).click();
        return this;
    }

    public SearchPage clickFirstCard() {
        $(firstCardLink).click();
        return this;
    }
}
