package org.example;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.*;

public class VillasPage {

    public static By famousVillas = By.cssSelector("li[role='link']:first-child");
    public static By firstFamousVillasCounter = By.cssSelector("li[role='link']:first-child p:last-child");

    public Integer firstFamousPageCounter;

    public VillasPage clickFirstFamousVillas() {
        $(famousVillas).click();
        return this;
    }

    public VillasPage readFirstFamousPageCounter() {
        switchTo().window(1);
        firstFamousPageCounter = Integer.valueOf($(firstFamousVillasCounter).shouldBe(Condition.visible)
                .text().replaceAll("[^0-9]", ""));
        return this;
    }
}
