package org.example;

import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.*;

public class HotelPage {
    public static By labelHotelName = By.xpath("//div[@data-capla-component='b-property-web-property-page/PropertyHeaderName']/h2");
    public static By labelHotelStarsCount = By.xpath("//span[@data-testid='rating-stars' or @data-testid='rating-circles']/span");
    public static By labelHotelAverageRating = By.xpath("//div[@data-capla-component='b-property-web-property-page/PropertyReviewScoreRight']/div/div/div[1]");
    public static By labelHotelReviewsCount = By.xpath("//div[@data-capla-component='b-property-web-property-page/PropertyReviewScoreRight']/div/div/div[2]/div[2]");
    public static By labelHotelCost = By.cssSelector(".prco-valign-middle-helper");
    public static By allPhotosSpan = By.cssSelector("span[class='bh-photo-grid-thumb-more-inner']");
    public static By secondPhoto = By.xpath("(//a[@class='bh-photo-modal-grid-item-wrapper'])[2]");

    public HotelPage checkLabels(
            String name,
            String starsCount,
            String averageRating,
            String reviewCount,
            String cost,
            SoftAssertions assertions
    ) {
        // Тут страница начинает загружаться
        switchTo().window(1);
        assertions.assertThat(name).as("Неправильное название отеля")
                .isEqualToIgnoringCase($(labelHotelName).text());
        assertions.assertThat(starsCount).as("Неправильное количество звезд")
                .isEqualToIgnoringCase(String.valueOf($$(labelHotelStarsCount).size()));
        assertions.assertThat(averageRating).as("Неправильная средняя оценка")
                .isEqualToIgnoringCase($(labelHotelAverageRating).text());
        assertions.assertThat(reviewCount).as("Неправильное количество отзывов")
                .isEqualToIgnoringCase($(labelHotelReviewsCount).text());
        assertions.assertThat(cost).as("Неправильная цена отеля")
                .isEqualToIgnoringCase($(labelHotelCost).text());

        assertions.assertAll();
        switchTo().defaultContent();
        return this;
    }

    public HotelPage clickAllPhotos() {
        switchTo().window(2);
        $(allPhotosSpan).click();
        return this;
    }

    public HotelPage clickSecondPhoto() {
        $(secondPhoto).click();
        return this;
    }
}
