package org.example;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.*;


public class DefaultPage {

    public static String baseUrl = "https://www.booking.com/";
    public static By cookieAcceptButton = By.xpath("//button[@id='onetrust-accept-btn-handler']");
    public static By cityInput = By.cssSelector("#\\:Ra9\\:");
    public static By firstSuggestion = By.cssSelector("ul[data-testid='autocomplete-results'] li:first-child");
    public static By googleLoginWindowFrame = By.xpath("//div[@id='credential_picker_container']/iframe");
    public static By googleLoginWindowCloseButton = By.cssSelector("#close");
    public static By datePicker = By.cssSelector("div[data-testid='searchbox-datepicker-calendar']");
    public static By searchButton = By.cssSelector("button[type='submit']");
    public static By villasPageButton = By.xpath("//div[text()='Виллы']");

    public DefaultPage openSite() {
        open(baseUrl);
        return this;
    }

    public DefaultPage acceptCookies() {
        $(cookieAcceptButton).click();
        return this;
    }

    public DefaultPage searchCity(String city) {
        $(cityInput).setValue(city);
        $(firstSuggestion).click();
        return this;
    }

    public DefaultPage closeGoogleWindow() {
        switchTo().frame($(googleLoginWindowFrame));
        $(googleLoginWindowCloseButton).click();
        switchTo().defaultContent();
        return this;
    }

    public DefaultPage inputDate(String start, String end) {
        $(datePicker).find(By.cssSelector("span[data-date='%s']".formatted(start))).click();
        $(datePicker).find(By.cssSelector("span[data-date='%s']".formatted(end))).click();
        return this;
    }

    public DefaultPage clickSearch() {
        $(searchButton).click();
        return this;
    }

    public DefaultPage clickVillasPage() {
        $(villasPageButton).click();
        return this;
    }
}
