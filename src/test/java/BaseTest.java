import org.assertj.core.api.SoftAssertions;
import org.example.DefaultPage;
import org.example.HotelPage;
import org.example.SearchPage;
import org.example.VillasPage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.LocalDate;
import java.util.List;

import static com.codeborne.selenide.WebDriverRunner.closeWebDriver;
import static com.codeborne.selenide.WebDriverRunner.setWebDriver;

public class BaseTest {

    VillasPage villasPage = new VillasPage();
    DefaultPage defaultPage = new DefaultPage();
    SearchPage searchPage = new SearchPage();
    HotelPage hotelPage = new HotelPage();
    String searchCity = "Париж";
    LocalDate startDate = LocalDate.now().plusDays(2);
    LocalDate endDate = startDate.plusWeeks(2);
    List<Integer> filters = List.of(1, 3);

    SoftAssertions assertions = new SoftAssertions();

    @Before
    public void setUp() {
        setWebDriver(new ChromeDriver());
    }

    @After
    public void onStop() {
//        closeWebDriver();
        assertions.assertAll();
    }

    @Test
    public void firstTest() {
        defaultPage.openSite()                  // 1.1 зайти на сайт https://www.booking.com/
                .acceptCookies()                // 1.1.1 принять cookies
                .searchCity(searchCity)         // 1.2 ввести в поиске любой город(заграничный)
                .inputDate(                     // 1.3 выбрать случайные даты
                        startDate.toString(),
                        endDate.toString()
                )
                .clickSearch();                 // 1.4 нажать на кнопку «Найти»
        searchPage.openMap()                    // 1.5 нажать на кнопку «Показать на карте»
                .moveMouseToFirstHotel()        // 1.6 навести курсор на первый отель(карточка слева)
                .initHotelDataFromMap()         // 1.7 сохранить(в переменные) название отеля, количество звезд,
                                                // среднюю оценку, количество отзывов, стоимость
                .clickJumpingMarker();          // 1.8 нажать на прыгающий маркер на карте
        hotelPage.checkLabels(                  // 1.9 на открывшейся странице отеля проверить название отеля,
                searchPage.hotelName,           // количество звезд, среднюю оценку, количество отзывов и стоимость
                searchPage.hotelStarsCount,
                searchPage.hotelAverageRating,
                searchPage.hotelReviewsCount,
                searchPage.hotelCost,
                assertions
        );
    }

    @Test
    public void secondTest() {
        defaultPage.openSite()
                .acceptCookies()
                .clickVillasPage();
        villasPage.readFirstFamousPageCounter()
                .clickFirstFamousVillas();
        searchPage.inputDateAfterVillasPage(
                    startDate.toString(),
                    endDate.toString()
                )
                .checkVillasCounter(
                    villasPage.firstFamousPageCounter,
                    assertions
                )
                .sortByGradeDescending()
                .setFilters(filters)
                .clickFirstCard();
        hotelPage.clickAllPhotos()
                .clickSecondPhoto();
    }
}
